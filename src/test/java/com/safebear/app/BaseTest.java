package com.safebear.app;


import org.junit.After;
import org.junit.Before;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class BaseTest {

    WebDriver driver;

    @Before
    public void setUp(){
        driver = new ChromeDriver();
        driver.get("http://automate.safebear.co.uk");
        driver.manage().window().maximize();



    }

    @After
    public void tearDown(){

    }
}
